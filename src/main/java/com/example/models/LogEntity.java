package com.example.models;


import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name = "log")
public class LogEntity implements Serializable {

    @Id
    private int id;

    @Column(name = "entity")
    private String entity;

    @Column(name = "entity_id")
    private int entityId;

    @Column(name = "is_deleted", columnDefinition = "TINYINT")
    private Boolean isDeleted;

    @OneToMany(mappedBy = "log")
    @Where(clause = "entity = 'CUSTOMER'")
    @JsonManagedReference
    private List<CustomerEntity> customer;
}
