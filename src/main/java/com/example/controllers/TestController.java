package com.example.controllers;


import com.example.models.CustomerEntity;
import com.example.models.LogEntity;
import com.example.repositories.LogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @Autowired
    private LogRepository logRepository;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public LogEntity getLog(){
        return logRepository.findOne(1);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public CustomerEntity getLog(@PathVariable("id") CustomerEntity customerEntity){
        return customerEntity;
    }
}
